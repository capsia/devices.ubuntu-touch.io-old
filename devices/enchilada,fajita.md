---
codename: "enchilada, fajita"
name: "OnePlus 6/6T"
comment: "community device"
deviceType: "phone"
noInstall: true
maturity: .7

deviceInfo:
  -
    id: 'cpu'
    value: 'Octa-core (4x 2.8 GHz Kryo 385 Gold & 4x 1.7 GHz Kryo 385 Silver)'
  -
    id: 'chipset'
    value: 'Qualcomm SDM845 Snapdragon 845'
  -
    id: 'gpu'
    value: 'Adreno 630'
  -
    id: 'rom'
    value: '128/256 GB UFS2.1'
  -
    id: 'ram'
    value: '6/8 GB LPDDR4X'
  -
    id: 'battery'
    value: 'Non-removable Li-Po 3400 / 3700 mAh'
  -
    id: 'rearCamera'
    value: '16 MP'

contributors:
  -
    name: 'calebccff'
    photo: ''
    forum: '#'
  -
    name: 'MrCyjanek'
    photo: ''
    forum: '#'

externalLinks:
  -
    name: 'Kernel sources'
    link: 'https://gitlab.com/ubports/community-ports/android9/oneplus-6/kernel-oneplus-sdm845'
    icon: 'github'
  -
    name: 'Device overlay'
    link: 'https://gitlab.com/ubports/community-ports/android9/oneplus-6/oneplus-enchilada-fajita'
    icon: 'github'
---

### Issues

* Poor idle drain due to `CONFIG_PM_AUTOSLEEP` being disabled to prevent crashes.
* Camera doesn't seem to function?
* SIM slot 1 doesn't work, workaround by moving your SIM card to slot 2.

