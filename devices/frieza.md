---
codename: 'frieza'
name: 'Bq Aquaris M10 FHD'
deviceType: 'tablet'
maturity: .999

installHelp:
  default: false
---

Bq m10 FHD devices that are sold with Android have a locked bootloader, so those need to be [manually unlocked by installing the manufacturers Ubuntu image](https://docs.ubports.com/en/latest/userguide/install.html#install-on-legacy-android-devices) before switching to UBports' release of Ubuntu Touch.
