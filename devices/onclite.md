---
codename: 'onclite'
name: 'Xiaomi Redmi 7'
comment: ''
deviceType: 'phone'
noInstall: true
installLink: 'https://github.com/Vin4ter/Ubports-Onclite-redmi7/blob/master/README.md'
maturity: .96
---
