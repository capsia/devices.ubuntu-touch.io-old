---
codename: 'rpi'
name: 'Raspberry Pi'
comment: 'experimental'
deviceType: 'tv'
noInstall: true
installLink: 'https://gitlab.com/ubports/community-ports/raspberrypi'
maturity: .1
---

The [Raspberry Pi](https://www.raspberrypi.org/) is an affordable ARM-based linux single-board computer. An [experimental Ubuntu Touch image](https://ci.ubports.com/job/rootfs/job/rootfs-rpi/) is available.
