---
name: 'Pinephone'
codename: 'pinephone'
deviceType: 'phone'
portType: 'Native'
maturity: 0.75
noInstall: true
installLink: 'https://gitlab.com/ubports/community-ports/pinephone'
tag: 'promoted'
deviceInfo:
  -
    id: 'cpu'
    value: '4x 1152 MHz Cortex-A53'
  -
    id: 'chipset'
    value: 'Allwinner A64'
  -
    id: 'gpu'
    value: 'Mali-400 MP2'
  -
    id: 'rom'
    value: '16 GB / 32 GB'
  -
    id: 'ram'
    value: '2 GB / 3 GB'
  -
    id: 'battery'
    value: '???'
  -
    id: 'display'
    value: '5.95" 720x1440 IPS'
  -
    id: 'rearCamera'
    value: 'Single 5MP, 1/4″, LED Flash'
  -
    id: 'frontCamera'
    value: 'Single 2MP, f/2.8, 1/5″'
  -
    id: 'arch'
    value: 'AArch64'
  -
    id: 'dimensions'
    value: '160.5mm x 76.6mm x 9.2mm'
externalLinks:
  -
    name: 'Pine64 subforum'
    link: 'https://forum.pine64.org/forumdisplay.php?fid=125'
    icon: 'rss'
  -
    name: 'UBports subforum'
    link: 'https://forums.ubports.com/topic/2403/pinephone'
    icon: 'yumi'
  -
    name: 'Pinephone subreddit'
    link: 'https://www.reddit.com/r/pinephone/'
    icon: 'rss'
  -
    name: 'IRC ( #pinephone on irc.pine64.org )'
    link: 'https://www.pine64.org/web-irc/'
    icon: 'irc'
  -
    name: '#pinephone on Pine64 Discord'
    link: 'https://discord.com/invite/DgB7kzr'
    icon: 'rss'
  -
    name: '#pinephone:matrix.org'
    link: '#'
    icon: 'rss'
  -
    name: 'Telegram chat'
    link: 'https://t.me/pinephone'
    icon: 'telegram'
  -
    name: 'Source repository'
    link: 'https://gitlab.com/ubports/community-ports/pinephone'
    icon: 'github'
  -
    name: 'Builds'
    link: 'https://ci.ubports.com/job/rootfs/job/rootfs-pinephone/'
    icon: 'yumi'
---
