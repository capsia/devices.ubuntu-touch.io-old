---
codename: 'h918'
name: 'LG V20 T-Mobile'
comment: 'wip'
deviceType: 'phone'
noInstall: true
maturity: 0

externalLinks:
  -
    name: 'Forum Post'
    link: 'https://forums.ubports.com/topic/3367/lg-v20-t-mobile-h918'
    icon: 'yumi'
---
