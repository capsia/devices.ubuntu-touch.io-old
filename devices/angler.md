---
codename: 'angler'
name: 'Nexus 6P'
deviceType: 'phone'
portType: 'Halium 7.1'
maturity: .7

externalLinks:
  -
    name: 'Forum Post'
    link: 'https://forums.ubports.com/topic/3678/call-for-testing-google-huawei-nexus-6p-angler-owners'
    icon: 'yumi'

noInstall: true
---
