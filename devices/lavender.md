---
codename: 'lavender'
name: 'Redmi Note 7'
comment: 'wip'
deviceType: 'phone'
noInstall: true
maturity: 0

externalLinks:
  -
    name: 'Forum Post'
    link: 'https://github.com/ubports-lavender'
    icon: 'yumi'
---
