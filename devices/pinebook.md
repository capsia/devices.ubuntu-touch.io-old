---
codename: 'pinebook'
name: 'Pinebook'
deviceType: 'laptop'
noInstall: true
maturity: .25
---

The [Pinebook](https://www.pine64.org/pinebook/) is an affordable ARM-based linux laptop. An [experimental Ubuntu Touch image](https://ci.ubports.com/job/rootfs/job/rootfs-pinebook/) is available.
