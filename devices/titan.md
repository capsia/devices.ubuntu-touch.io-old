---
codename: 'titan'
name: 'Moto G (2014)'
comment: 'community device'
deviceType: 'phone'
noInstall: true
installLink: 'https://forum.xda-developers.com/moto-g-2014/development/experimental-ubuntu-touch-titan-t3608846'
maturity: .1
---
