---
codename: 'cancro'
name: 'Xiaomi 4'
deviceType: 'phone'
noInstall: true
installLink: 'https://gitlab.com/ubports/community-ports/cancro'
maturity: 0

externalLinks:
  -
    name: 'Repository'
    link: 'https://gitlab.com/ubports/community-ports/cancro'
    icon: 'github'
---
