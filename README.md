# Ubuntu Touch devices

The new new [Ubuntu Touch devices website](https://devices.ubuntu-touch.io)! Made using [Gridsome](https://gridsome.org/). You can start a development server by running `gridsome develop`.

## Adding new devices

You can add new devices by adding a markdown file with a YAML-metadata-block under `/devices/<codename>.md`.

The YAML-metatdata-block should have the following format; variables in square brackets are `[optional]`:

```yml
name: <retail name of the device>
codename: <primary codename of the device>
deviceType: <phone|tablet|tv|other>
maturity: <number between 0 and 1 indicating quality of the port>
[noinstall: <set to true to hide the installer block>]
[installLink: <link to install instructions (repo), if installer isn't available>]
[tag: <prometed|old>]
[
portStatus:
  -
    categoryName: <name of the category of the features>
    features:
      -
        id: <feature ID, you can find the complete list below>
        value: < + | - >
        bugtracker: <link to the bugtracker>
]
[
deviceInfo:
  -
    id: <device specification ID, you can find the complete list below>
    value: <device technical specification, model...>
]
[
contributors:
  -
    name: <contributor name>
    photo: <link to avatar picture>
    forum: <link to ubports forum profile>
]
[
externalLinks:
  -
    name: <link label>
    link: <target>
    icon: <icon for the link>
]
installHelp:
  default: <true : use default install instructions |false : write install instructions in the instructions field>
[  rebootToBootloader: <buttons to press to reboot to bootloader>]
[  instructions: <html block to be displayed in the instructions section>]
```

### Feature IDs

Actors

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| manualBrightness     | Manual brightness                       |
| notificationLed      | Notification LED                        |
| torchlight           | Torchlight                              |
| vibration            | Vibration                               |

Camera

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| flashlight           | Flashlight                              |
| photo                | Photo                                   |
| video                | Video                                   |
| switchCamera         | Switching between cameras               |

Cellular

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| carrierInfo          | Carrier info, signal strength           |
| dataConnection       | Data connection                         |
| calls                | Incoming, outgoing calls                |
| mms                  | MMS in, out                             |
| pinUnlock            | PIN unlock                              |
| sms                  | SMS in, out                             |
| audioRoutings        | Change audio routings                   |
| voiceCall            | Voice in calls                          |

Endurance

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| batteryLifetimeTest  | Battery lifetime > 24h from 100%        |
| noRebootTest         | No reboot needed for 1 week             |

GPU

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| uiBoot               | Boot into UI                            |
| videoAcceleration    | Video acceleration                      |

Misc

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| anboxPatches         | Anbox patches                           |
| apparmorPatches      | AppArmor patches                        |
| batteryPercentage    | Battery percentage                      |
| offlineCharging      | Offline charging                        |
| onlineCharging       | Online charging                         |
| recoveryImage        | Recovery image                          |
| factoryReset         | Reset to factory defaults               |
| rtcTime              | RTC time                                |
| sdCard               | SD card detection and access            |
| shutdown             | Shutdown / Reboot                       |
| wirelessCharging     | Wireless charging                       |

Network

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| bluetooth            | Bluetooth                               |
| flightMode           | Flight mode                             |
| hotspot              | Hotspot                                 |
| nfc                  | NFC                                     |
| wifi                 | WiFi                                    |

Sensors

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| autoBrightness       | Automatic brightness                    |
| fingerprint          | Fingerprint reader                      |
| gps                  | GPS                                     |
| proximity            | Proximity                               |
| rotation             | Rotation                                |
| touchscreen          | Touchscreen                             |

Sound

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| earphones            | Earphones                               |
| loudspeaker          | Loudspeaker                             |
| microphone           | Microphone                              |
| volumeControl        | Volume control                          |

USB

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| mtp                  | MTP access                              |
| adb                  | ADB access                              |
| externalMonitor      | External monitor                        |

### Device specification IDs

| Specification ID     | Specification name                      |
| :------------------- | :-------------------------------------: |
| cpu                  | CPU                                     |
| chipset              | Chipset                                 |
| gpu                  | GPU                                     |
| rom                  | ROM                                     |
| ram                  | RAM                                     |
| android              | Android Version                         |
| battery              | Battery                                 |
| display              | Display                                 |
| rearCamera           | Rear Camera                             |
| frontCamera          | Front Camera                            |
| architecture         | Architecture                            |
| dimensions           | Dimensions                              |

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
